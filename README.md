# emojis custom pour Mattermost
On a honteusement pompé les emojis custom de Framateam puis ajouté les notres.


## Comment installer des emojis custom par défaut ?
Les emojis ajoutés par quelqu'un sont visibles par tout le monde par défaut (c'est quelque chose qu'il faut monitorer d'ailleurs, pour éviter les soucis). Cf. la [discussion ouverte chez Framasoft](https://framagit.org/framasoft/framateam/-/issues/18).

### Donwload
Pour télécharger depuis un mattermost, on peut faire clic-droit > enregistrer l'image.

### Upload
Pour télécharger vers Mattermost, on peut soit faire à la main (en cliquant sur emoji personnalisés depuis la boîte de sélection d'emojis). 

Pour uploader des emojis *en masse*, on teste [mmemoji](https://github.com/maxbrunet/mmemoji).

Notre usage : (avec un dossier ./emoji/)

```
export MM_URL='http://blabla.girofle.cloud:8065/api/v4'
export MM_LOGIN_ID='<user>'
export MM_PASSWORD='<pass>
mmemoji create --no-clobber emoji/* # peut être /home/<user>/.local/bin/mmemoji si install locale
```
